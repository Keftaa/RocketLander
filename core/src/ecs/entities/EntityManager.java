package ecs.entities;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

import ecs.components.*;

public class EntityManager {

	public Entity player;
	private FitViewport viewport;
	private Entity[] stars;
	private Engine engine;
	public int starsCount;
	
	public EntityManager(FitViewport viewport, Engine engine) {
		this.viewport = viewport;
		this.engine = engine;
	}

	public void initPlayer() {
		player = new Entity();
		FuelComponent fuelComp = new FuelComponent();
		fuelComp.reservoirCapacity = 100;
		fuelComp.reservoirLeft = 100;
		fuelComp.lossPerSecond = 20;
		GravityPullComponent gravityComp = new GravityPullComponent();
		gravityComp.gravityY = -60;
		PositionComponent positionComp = new PositionComponent();
		positionComp.position = new Vector2(viewport.getWorldWidth() / 2,
				viewport.getWorldHeight());
		SpeedComponent speedComp = new SpeedComponent();
		speedComp.speedX = 100f;
		speedComp.speedY = 140f;
		speedComp.velocityX = 0f;
		speedComp.velocityY = 0f;
		speedComp.accelerationX = 30f;
		speedComp.accelerationY = 100f;
		SpriteComponent spriteComp = new SpriteComponent();
		spriteComp.sprite = new Sprite(new Texture(
				Gdx.files.internal("rocket.png")));
		spriteComp.otherSprite = new Sprite(new Texture(
				Gdx.files.internal("rocketThrust.png")));
		SizeComponent sizeComp = new SizeComponent();
		sizeComp.width = 2;
		sizeComp.height = 6;
		ControllableComponent controlComp = new ControllableComponent();
		CollisionComponent collisionComp = new CollisionComponent();
		collisionComp.vertices = new float[6];
		collisionComp.vertices[0] = 0;
		collisionComp.vertices[1] = 0;
		collisionComp.vertices[2] = sizeComp.width;
		collisionComp.vertices[3] = 0;
		collisionComp.vertices[4] = sizeComp.width / 2;
		collisionComp.vertices[5] = sizeComp.height;
		collisionComp.sensorPolygon = new Polygon();
		collisionComp.sensorPolygon.setVertices(collisionComp.vertices);
		collisionComp.rotationSensors = new Vector2[2];
		collisionComp.rotationSensors[0] = positionComp.position;
		collisionComp.rotationSensors[1] = new Vector2(positionComp.position.x
				+ sizeComp.width, positionComp.position.y);
		collisionComp.collidingRotationSensors = new boolean[2];
		ScoreComponent scoreComp = new ScoreComponent();
		player.add(fuelComp);
		player.add(gravityComp);
		player.add(positionComp);
		player.add(speedComp);
		player.add(spriteComp);
		player.add(sizeComp);
		player.add(controlComp);
		player.add(collisionComp);
		player.add(scoreComp);

	}

	public void reset() {
		initPlayer();
		removeStars();
		createStars();
	}

	public void createStars() {
		stars = new Entity[starsCount];
		for (int i = 0; i < starsCount; i++) {
			Entity star = new Entity();
			PositionComponent starPosition = new PositionComponent();
			starPosition.position = new Vector2(MathUtils.random(viewport
					.getWorldWidth()), MathUtils.random(viewport
					.getWorldHeight()));
			SizeComponent starSize = new SizeComponent();
			starSize.width = 3f;
			starSize.height = 3f;
			SpriteComponent starSprite = new SpriteComponent();
			starSprite.sprite = new Sprite(new Texture(
					Gdx.files.internal("star.png")));
			star.add(starPosition);
			star.add(starSize);
			star.add(starSprite);
			stars[i] = star;
			engine.addEntity(star);
		}
	}
	
	private void removeStars(){
		for(int i = 0; i < stars.length; i++){
			engine.removeEntity(stars[i]);
			stars[i] = null;
		}
	}

}
