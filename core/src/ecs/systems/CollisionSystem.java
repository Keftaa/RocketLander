package ecs.systems;

import com.badlogic.ashley.core.Engine;
import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.Intersector;
import com.badlogic.gdx.math.Vector2;

import ecs.components.*;

public class CollisionSystem extends IteratingSystem {

	private Mapper mapper;
	public Vector2[] linesPoints;
	private ShapeRenderer renderer;

	public CollisionSystem(Mapper mapper, Vector2[] linesPoints,
			ShapeRenderer renderer) {
		super(Family.all(CollisionComponent.class, SpeedComponent.class).get());
		this.mapper = mapper;
		this.linesPoints = linesPoints;
		this.renderer = renderer;
	}
	
	

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		CollisionComponent collision = mapper.collisions.get(entity);
		PositionComponent position = mapper.positions.get(entity);
		SizeComponent size = mapper.sizes.get(entity);
		SpeedComponent speed = mapper.speeds.get(entity);
		collision.sensorPolygon.setPosition(position.position.x,
				position.position.y);
		collision.rotationSensors[1].set(position.position.x+size.width, position.position.y);

		if (collision.isColliding){
			if(Math.abs(speed.lastVelocityY)>30)
				collision.outCome = CollisionComponent.CRASH;
			else{
				for(boolean b: collision.collidingRotationSensors){
					if(!b){
						collision.outCome = CollisionComponent.NOT_FLAT;
						break;
					}
					collision.outCome = CollisionComponent.PERFECT;
					speed.isAcceleratingUp = false;
					entity.remove(ControllableComponent.class);
					
				}
			}
				
			return;
		}
		for (int i = 0; i < linesPoints.length - 1; i++) {
			collision.isColliding = Intersector
					.intersectSegmentPolygon(linesPoints[i],
							linesPoints[i + 1], collision.sensorPolygon);
			if(collision.isColliding){
				collision.collidingPoint = i;
			}
			for (int j = 0; j < collision.rotationSensors.length; j++) {
				if (Intersector.distanceSegmentPoint(linesPoints[i],
						linesPoints[i + 1], collision.rotationSensors[j]) < 0.9f){
					collision.collidingRotationSensors[j] = true;
				}
				else{
					collision.collidingRotationSensors[j] = false;
				}
			}
			if (collision.isColliding)
				break;
			//renderer.begin(ShapeType.Line);
			//renderer.setColor(Color.YELLOW);
			//for(int j = 0; j < linesPoints.length - 1; j++){
			//	renderer.line(linesPoints[j], linesPoints[j+1]);
			//}
			//renderer.end();
		}

	}

}