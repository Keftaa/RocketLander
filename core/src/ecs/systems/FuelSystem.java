package ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import ecs.components.FuelComponent;
import ecs.components.SpeedComponent;

public class FuelSystem extends IteratingSystem {
	private Mapper mapper;
	public FuelSystem(Mapper mapper) {
		super(Family.all(SpeedComponent.class, FuelComponent.class).get());
		this.mapper = mapper;
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		FuelComponent fuel = mapper.fuels.get(entity);
		SpeedComponent speed = mapper.speeds.get(entity);
		if(speed.isAcceleratingUp)
			fuel.reservoirLeft -= fuel.lossPerSecond*deltaTime;
		if(fuel.reservoirLeft<0)
			entity.remove(FuelComponent.class);
	}

}
