package ecs.systems;

import java.util.ArrayList;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;

import ecs.components.*;

public class ScoreSystem extends IteratingSystem {
	private Mapper mapper;
	public ArrayList<Integer> multipliers;

	@SuppressWarnings("unchecked")
	public ScoreSystem(Mapper mapper, ArrayList<Integer> multipliers) {
		super(Family.all(ScoreComponent.class).one(CollisionComponent.class)
				.get());
		this.mapper = mapper;
		this.multipliers = multipliers;
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		ScoreComponent score = mapper.scrores.get(entity);
		CollisionComponent collision = mapper.collisions.get(entity);
		FuelComponent fuel = mapper.fuels.get(entity);
		if (collision != null && collision.collidingPoint >= 0) {
			score.multiplier = multipliers.get(collision.collidingPoint);
			if(collision.outCome==CollisionComponent.PERFECT){
				score.score = (int) (100*score.multiplier);
				if(fuel!=null){
					score.score += fuel.reservoirLeft*score.multiplier;
				}
			}
			
		}

	}

}
