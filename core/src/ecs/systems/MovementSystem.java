package ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.utils.viewport.FitViewport;

import ecs.components.CollisionComponent;
import ecs.components.GravityPullComponent;
import ecs.components.PositionComponent;
import ecs.components.SpeedComponent;
import ecs.components.*;

public class MovementSystem extends IteratingSystem {
	private Mapper mapper;
	private FitViewport viewport;

	@SuppressWarnings("unchecked")
	public MovementSystem(Mapper mapper, FitViewport viewport) {
		super(Family.all(PositionComponent.class, SpeedComponent.class,
				GravityPullComponent.class, SizeComponent.class,
				FuelComponent.class).get());
		this.mapper = mapper;
		this.viewport = viewport;
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		CollisionComponent collision = mapper.collisions.get(entity);
		SpeedComponent speed = mapper.speeds.get(entity);
		PositionComponent position = mapper.positions.get(entity);
		SizeComponent size = mapper.sizes.get(entity);
		GravityPullComponent gravity = mapper.gravities.get(entity);
		if(!collision.isColliding)
			speed.lastVelocityY = speed.velocityY;
		if (collision != null && collision.isColliding) {
			speed.velocityY = 0f;
			if (collision.collidingRotationSensors[0]) {
				position.originX = 0;
				position.originY = 0;
				position.angle -= 12 * deltaTime;
			}
			if (collision.collidingRotationSensors[1]) {
				position.originX = size.width;
				position.originY = 0;
				position.angle += 12 * deltaTime;
			}
			return;
		}

		if (speed.isAcceleratingRight)
			speed.velocityX += (speed.accelerationX + gravity.gravityX)
					* deltaTime;
		if (speed.isAcceleratingLeft)
			speed.velocityX -= (speed.accelerationX + gravity.gravityX)
					* deltaTime;
		if (speed.isAcceleratingUp) {
			speed.velocityY += (speed.accelerationY + gravity.gravityY)
					* deltaTime;
		} else
			speed.velocityY += (gravity.gravityY) * deltaTime;
		
		if (speed.velocityX > Math.abs(speed.speedX))
			speed.velocityX = speed.speedX;
		// if(speed.velocityY > speed.speedY)
		// speed.velocityY = speed.speedY;
		position.position.x += speed.velocityX * 0.5f * deltaTime;
		position.position.y += speed.velocityY * 0.5f * deltaTime;
		if (position.position.x > viewport.getWorldWidth())
			position.position.x = 0;
		if (position.position.x < 0)
			position.position.x = viewport.getWorldWidth();

	}

}
