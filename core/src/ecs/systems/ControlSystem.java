package ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

import ecs.components.*;

public class ControlSystem extends IteratingSystem {
	private Mapper mapper;
	@SuppressWarnings("unchecked")
	public ControlSystem(Mapper mapper) {
		super(Family.all(ControllableComponent.class, SpeedComponent.class).get());
		this.mapper = mapper;
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		SpeedComponent speed = mapper.speeds.get(entity);
		if(Gdx.input.isKeyPressed(Input.Keys.UP)){
			speed.isAcceleratingUp = true;
		}
		else{
			speed.isAcceleratingUp = false;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.LEFT)){
			speed.isAcceleratingLeft = true;
		}
		else{
			speed.isAcceleratingLeft = false;
		}
		if(Gdx.input.isKeyPressed(Input.Keys.RIGHT)){
			speed.isAcceleratingRight = true;
		}
		else{
			speed.isAcceleratingRight = false;
		}

		
		
	}

}
