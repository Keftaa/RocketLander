package ecs.systems;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.Family;
import com.badlogic.ashley.systems.IteratingSystem;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;

import ecs.components.*;

public class SpriteRenderingSystem extends IteratingSystem {

	private SpriteBatch batch;
	private Mapper mapper;

	@SuppressWarnings("unchecked")
	public SpriteRenderingSystem(Mapper mapper, SpriteBatch batch) {
		super(Family.all(SpriteComponent.class, SizeComponent.class).get());
		this.batch = batch;
		this.mapper = mapper;
	}

	@Override
	protected void processEntity(Entity entity, float deltaTime) {
		SpriteComponent sprite = mapper.sprites.get(entity);
		SizeComponent size = mapper.sizes.get(entity);
		SpeedComponent speed = mapper.speeds.get(entity);

		PositionComponent position = mapper.positions.get(entity);
		sprite.sprite.setSize(size.width, size.height);
		sprite.sprite.setOrigin(position.originX, position.originY);
		sprite.sprite.setPosition(position.position.x, position.position.y);
		sprite.sprite.setRotation(position.angle);
		if (sprite.otherSprite != null) {
			sprite.otherSprite.setSize(size.width, size.height);
			sprite.otherSprite.setOrigin(position.originX, position.originY);
			sprite.otherSprite.setPosition(position.position.x,
					position.position.y);
			sprite.otherSprite.setRotation(position.angle);
		}
		if (speed != null) {
			if (speed.isAcceleratingUp){
				if(sprite.otherSprite!=null)
					sprite.otherSprite.draw(batch);
			}
			else
				sprite.sprite.draw(batch);
		} else
			sprite.sprite.draw(batch);
	}

}
