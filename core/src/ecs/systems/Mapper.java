package ecs.systems;

import com.badlogic.ashley.core.ComponentMapper;

import ecs.components.*;

public class Mapper {
	public ComponentMapper<SpeedComponent> speeds = ComponentMapper
			.getFor(SpeedComponent.class);
	public ComponentMapper<PositionComponent> positions = ComponentMapper
			.getFor(PositionComponent.class);
	public ComponentMapper<FuelComponent> fuels = ComponentMapper
			.getFor(FuelComponent.class);
	public ComponentMapper<GravityPullComponent> gravities = ComponentMapper
			.getFor(GravityPullComponent.class);
	public ComponentMapper<SizeComponent> sizes = ComponentMapper
			.getFor(SizeComponent.class);
	public ComponentMapper<SpriteComponent> sprites = ComponentMapper
			.getFor(SpriteComponent.class);
	public ComponentMapper<CollisionComponent> collisions = ComponentMapper
			.getFor(CollisionComponent.class);
	public ComponentMapper<ScoreComponent> scrores = ComponentMapper
			.getFor(ScoreComponent.class);

}
