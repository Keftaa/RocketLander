package ecs.components;

import com.badlogic.ashley.core.Component;
import com.badlogic.gdx.math.Polygon;
import com.badlogic.gdx.math.Vector2;

public class CollisionComponent implements Component {
	public static final int CRASH = 1;
	public static final int NOT_FLAT = 2;
	public static final int PERFECT = 3;
	public int outCome;
	public float[] vertices;
	public Polygon sensorPolygon;
	public boolean isColliding;
	public Vector2[] rotationSensors; // currently only position and position+width
	public boolean[] collidingRotationSensors;
	public int collidingPoint = -1;
}
