package ecs.components;

import com.badlogic.ashley.core.Component;

public class SpeedComponent implements Component {
	public float speedX, speedY, velocityX, velocityY, accelerationX,
			accelerationY;

	public boolean isAcceleratingLeft, isAcceleratingRight, isAcceleratingUp,
			isRotatingLeft, isRotatingRight;


	public float lastVelocityY;
	
}
