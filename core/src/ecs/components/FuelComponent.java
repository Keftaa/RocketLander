package ecs.components;

import com.badlogic.ashley.core.Component;

public class FuelComponent implements Component {
	public int reservoirCapacity;
	public float reservoirLeft;
	public int lossPerSecond;
}
