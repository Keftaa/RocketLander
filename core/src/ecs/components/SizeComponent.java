package ecs.components;

import com.badlogic.ashley.core.Component;

public class SizeComponent implements Component {
	public float width, height;
}
