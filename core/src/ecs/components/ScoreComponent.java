package ecs.components;

import com.badlogic.ashley.core.Component;

public class ScoreComponent implements Component {
	public float multiplier;
	public int score;
}
