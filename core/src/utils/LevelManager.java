package utils;

import java.io.FileInputStream;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Comparator;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.graphics.Pixmap.Blending;
import com.badlogic.gdx.graphics.Pixmap.Format;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.Gdx2DPixmap;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer.ShapeType;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Plane;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.viewport.FitViewport;

public class LevelManager {
	private ShapeRenderer renderer;
	private FitViewport viewport;
	public ArrayList<Integer> multipliers;
	private BitmapFont font;
	private ArrayList<String> multipliersText;
	private float playerWidth;
	private Pixmap map;
	private Texture moonTextureClean;
	private Sprite moonSprite;
	public Vector2[] positions;
	private int[][] pixelColors;
	private ArrayList<SurfaceInfo> surfacesInfos;

	public LevelManager(ShapeRenderer renderer, FitViewport viewport,
			float width) {
		// Pixmap.setBlending(Pixmap.Blending.None);
		this.renderer = renderer;
		this.viewport = viewport;
		font = new BitmapFont();
		font.setColor(Color.WHITE);
		this.playerWidth = width;
		// moonTextureClean = new Texture(Gdx.files.internal("moon.jpg"));
		map = new Pixmap(Gdx.files.internal("moon.jpg"));
		// First we get the pixmap from the moon picture
		// if (moonTextureClean.getTextureData().isPrepared() == false)
		// moonTextureClean.getTextureData().prepare();

		// map = moonTextureClean.getTextureData().consumePixmap();
		moonTextureClean = new Texture(map);
		pixelColors = new int[map.getWidth()][map.getHeight()];
		for (int i = 0; i < map.getWidth(); i++) {
			for (int j = map.getHeight() - 1; j >= 0; j--) {
				pixelColors[i][j] = map.getPixel(i, j);
			}
		}
	}

	public void generateVertices() {
		surfacesInfos = new ArrayList<SurfaceInfo>();
		int points = 50;
		positions = new Vector2[points];
		// we specify $points random points on the pixmap
		for (int i = 0; i < positions.length; i++) {
			positions[i] = new Vector2(MathUtils.random(map.getWidth()),
					MathUtils.random(map.getHeight()));

		}
		Comparator<Vector2> pointsComparator = new Comparator<Vector2>() {

			@Override
			public int compare(Vector2 p1, Vector2 p2) {
				return Float.compare(p1.x, p2.x);

			}

		};
		// We sort them on the x axis
		Arrays.sort(positions, pointsComparator);

		// We check whether we have two consecutive points that are on the same
		// Y
		// (aka check whether we have a surface or not)
		int surfaces = 0;
		for (int i = 0; i < positions.length - 1; i++) {
			if (positions[i].y == positions[i + 1].y
					&& positions[i + 1].x - positions[i].x > playerWidth*4) {
				surfaces++;
			}
		}

		// If we find that we have less than $surfaces surfaces, we generate
		// some more
		if (surfaces < 5) {
			surfaces = 20;
			for (int i = 0; i < surfaces; i++) {
				int randomPoint = MathUtils.random(0, positions.length - 2);
				if (positions[randomPoint].y - positions[randomPoint + 1].y != 0
						&& positions[randomPoint].y
								- positions[randomPoint
										- (randomPoint == 0 ? 0 : 1)].y != 0) {
					if (Math.abs(positions[randomPoint].x
							- positions[randomPoint + 1].x) > playerWidth*4) {
						positions[randomPoint].y = positions[randomPoint + 1].y;
					}
				}

			}
		}

		// We put black on top of the whole pixmap
		map.setColor(Color.alpha(1));
		map.fillRectangle(0, 0, map.getWidth(), map.getHeight());

		// for each two consecutive points
		for (int p = 0; p < positions.length - 1; p++) {
			float x1 = positions[p].x;
			float x2 = positions[p + 1].x;
			float y1 = positions[p].y;
			float y2 = positions[p + 1].y;
			float a = (y2 - y1) / (x2 - x1); // y = ax+b
			for (int i = (int) x1; i < x2; i++) {
				int j = (int) (a * i + (y1 - a * x1));
				for (int jj = j; jj < j + 35; jj++) {
					map.drawPixel(i, jj, Color.rgba8888(1, 1, 1, 0.1f));
				}
				// We got on straight vertical lines to redraw the original
				// pixel color in that region
				for (j = j + 35; j < map.getHeight(); j++) {
					map.drawPixel(i, j, pixelColors[i][j]);
				}
			}
		}
		// we draw the pixmap on our tezxture
		moonTextureClean.draw(map, 0, 0);
		// wrap everything into a sprite
		moonSprite = new Sprite(moonTextureClean);
		moonSprite.setPosition(0, 0);
		// we calculate the modification ratio of the size of the sprite so that
		// we
		// can find the points relative to the screen rather than the pixmap
		float xTranslation = moonSprite.getWidth() / viewport.getWorldWidth();
		float yTranslation = moonSprite.getHeight()
				/ (viewport.getWorldHeight() / 2);
		moonSprite.setSize(viewport.getWorldWidth(),
				viewport.getWorldHeight() / 2);
		// we set the positions to the screen (world) positions
		for (int i = 0; i < positions.length; i++) {
			positions[i].x = positions[i].x / xTranslation;
			positions[i].y = (map.getHeight() - positions[i].y) / yTranslation;
		}
		for(int i = 0; i < positions.length-1; i++){
			if(positions[i+1].y - positions[i].y == 0){
				if(positions[i+1].x - positions[i].x > playerWidth){
					surfacesInfos.add(new SurfaceInfo(new Vector2(positions[i]),
							Math.abs(positions[i].x - positions[i + 1].x)));
		
				}
			}
		}
		setSurfacesMultipliers();
		setMultipliersText();

	}

	private void setSurfacesMultipliers() {
		multipliers = new ArrayList<Integer>();
		for (int i = 0; i < positions.length - 1; i++) {
			if (positions[i].y == positions[i + 1].y)
				multipliers.add((int) Math.abs(30 - Math.abs(positions[i].x
						- positions[i + 1].x)-(positions[i].y/6))*100);
			else
				multipliers.add(0);
		}
	}

	private void setMultipliersText() {
		multipliersText = new ArrayList<String>();
		for (int i = 0; i < multipliers.size(); i++) {
			multipliersText.add("x" + multipliers.get(i));
		}
	}

	public void renderMultipliers(SpriteBatch batch) {
		for (int i = 0; i < positions.length - 1; i++) {
			if (positions[i].y == positions[i + 1].y) {
				float distance = Math.abs(positions[i].x - positions[i + 1].x);
				font.draw(batch, multipliersText.get(i), positions[i].x * 4
						+ distance / 2, positions[i].y * 4 + 20);
			}
		}

	}

	float colorTimer = 0;
	Color color = Color.RED;
	float dotTimer = 0;

	public void render(SpriteBatch batch) {
		// renderer.begin(ShapeType.Point);
		batch.begin();
		moonSprite.draw(batch);
		batch.end();
		renderer.begin(ShapeType.Point);
		colorTimer += Gdx.graphics.getDeltaTime();
		if (colorTimer > 0.5f) {
			if (color == Color.RED)
				color = Color.WHITE;
			else
				color = Color.RED;
			renderer.setColor(color);
			colorTimer = 0;
		}
		for (int i = 0; i < surfacesInfos.size(); i++) {
			float x = surfacesInfos.get(i).startingPoint.x;
			while(x<surfacesInfos.get(i).length+surfacesInfos.get(i).startingPoint.x){
				x+=0.3f;
				renderer.point(x, surfacesInfos.get(i).startingPoint.y, 0);
			}
		}
		renderer.end();
	}

	class SurfaceInfo {
		public Vector2 startingPoint;
		public float length;
		public float timer;

		SurfaceInfo(Vector2 startingPoint, float length) {
			this.startingPoint = startingPoint;
			this.length = length;
			timer = 0;
		}
	}
}
