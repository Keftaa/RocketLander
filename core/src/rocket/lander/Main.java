package rocket.lander;

import screens.GameScreen;

import com.badlogic.ashley.core.Engine;
import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.viewport.FitViewport;
public class Main extends Game {
	public final static float WORLD_WIDTH = 200;
	public final static float WORLD_HEIGHT = 90;
	
	public SpriteBatch batch;
	public Engine engine;
	public FitViewport hudViewport;
	public SpriteBatch hudBatch;
	@Override
	public void create () {
		batch = new SpriteBatch();
		hudBatch = new SpriteBatch();
		engine = new Engine();
		hudViewport = new FitViewport(WORLD_WIDTH*4, WORLD_HEIGHT*4);
		this.setScreen(new GameScreen(this));
	}
}