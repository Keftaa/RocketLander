package screens;

import java.sql.Date;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.HashMap;

import rocket.lander.Main;
import utils.LevelManager;

import com.badlogic.ashley.core.Entity;
import com.badlogic.ashley.core.EntitySystem;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.audio.Sound;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.GlyphLayout;
import com.badlogic.gdx.graphics.glutils.ShaderProgram;
import com.badlogic.gdx.graphics.glutils.ShapeRenderer;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.utils.viewport.FitViewport;

import ecs.components.CollisionComponent;
import ecs.components.FuelComponent;
import ecs.components.ScoreComponent;
import ecs.components.SizeComponent;
import ecs.components.SpeedComponent;
import ecs.entities.EntityManager;
import ecs.systems.*;

public class GameScreen implements Screen {
	private Main main;
	private FitViewport viewport;
	private ShapeRenderer shapeRenderer;
	private LevelManager levelManager;
	private EntityManager entityManager;
	private Mapper mapper;
	private HeadsUpDisplay hud;
	private ShaderProgram shader;

	public GameScreen(Main main) {
		this.main = main;

	}

	@Override
	public void show() {
		ShaderProgram.pedantic = false;
		shader = new ShaderProgram(
				Gdx.files.internal("shaders/passthrough.vsh"),
				Gdx.files.internal("shaders/passthrough.fsh"));
		if (shader.isCompiled() == false)
			System.out.println(shader.getLog());
		main.batch.setShader(shader);
		viewport = new FitViewport(Main.WORLD_WIDTH, Main.WORLD_HEIGHT);
		shapeRenderer = new ShapeRenderer();
		mapper = new Mapper();
		entityManager = new EntityManager(viewport, main.engine);
		entityManager.initPlayer();
		entityManager.starsCount = 25;
		entityManager.createStars();
		main.engine.addEntity(entityManager.player);
		levelManager = new LevelManager(shapeRenderer, viewport,
				entityManager.player.getComponent(SizeComponent.class).width);
		levelManager.generateVertices();
		main.engine.addSystem(new CollisionSystem(mapper,
				levelManager.positions, shapeRenderer));
		main.engine.addSystem(new MovementSystem(mapper, viewport));
		main.engine.addSystem(new SpriteRenderingSystem(mapper, main.batch));
		main.engine.addSystem(new ControlSystem(mapper));
		main.engine
				.addSystem(new ScoreSystem(mapper, levelManager.multipliers));
		main.engine.addSystem(new FuelSystem(mapper));
		hud = new HeadsUpDisplay(entityManager.player);
	}

	@Override
	public void render(float delta) {
		Gdx.gl.glClearColor(0, 0, 0, 1);
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);
		main.hudBatch
				.setProjectionMatrix(main.hudViewport.getCamera().combined);
		main.hudViewport.apply(true);
		main.batch.setProjectionMatrix(viewport.getCamera().combined);
		viewport.apply(true);

		shapeRenderer.setProjectionMatrix(viewport.getCamera().combined);
		main.engine.getSystem(CollisionSystem.class).update(delta);
		main.batch.begin();
		for (EntitySystem system : main.engine.getSystems()) {
			if (system != main.engine.getSystem(CollisionSystem.class))
				system.update(delta);
		}
		main.batch.end();
		levelManager.render(main.batch);
		main.hudBatch.begin();
		levelManager.renderMultipliers(main.hudBatch);
		hud.render();
		main.hudBatch.end();
		if (Gdx.input.isKeyJustPressed(Input.Keys.R)) {
			main.engine.removeEntity(entityManager.player);
			entityManager.reset();
			main.engine.addEntity(entityManager.player);
			hud.setEntity(entityManager.player);
			hud.typingTimer = 0;
			hud.typingTimerSound = 0;
			levelManager.generateVertices();
			main.engine.getSystem(CollisionSystem.class).linesPoints = levelManager.positions;
			main.engine.getSystem(ScoreSystem.class).multipliers = levelManager.multipliers;
		}

	}

	@Override
	public void resize(int width, int height) {
		viewport.update(width, height);
		main.hudViewport.update(width, height);
		main.hudViewport.apply(true);
	}

	@Override
	public void pause() {
		// TODO Auto-generated method stub

	}

	@Override
	public void resume() {
		// TODO Auto-generated method stub

	}

	@Override
	public void hide() {
		// TODO Auto-generated method stub

	}

	@Override
	public void dispose() {
		// TODO Auto-generated method stub

	}

	class HeadsUpDisplay {
		BitmapFont font, bigFont;
		ArrayList<Vector2> positions;
		Entity entity;
		SpeedComponent speed;
		FuelComponent fuel;
		ScoreComponent score;
		CollisionComponent collision;
		float lastY;
		String resultMessage;
		GlyphLayout glyphLayout;
		private Sound typingSound, typingSound2, typingSound3, typingSound4;

		HeadsUpDisplay(Entity entity) {
			this.entity = entity;
			positions = new ArrayList<Vector2>();
			bigFont = new BitmapFont(Gdx.files.internal("big.fnt"));
			font = new BitmapFont();
			font.setColor(Color.WHITE);
			speed = entity.getComponent(SpeedComponent.class);
			fuel = entity.getComponent(FuelComponent.class);
			lastY = main.hudViewport.getWorldHeight();
			resultMessage = "";
			glyphLayout = new GlyphLayout();
			glyphLayout.setText(bigFont, resultMessage);
			collision = entity.getComponent(CollisionComponent.class);
			score = entity.getComponent(ScoreComponent.class);
			typingSound = Gdx.audio.newSound(Gdx.files.internal("typing.wav"));
			typingSound2 = Gdx.audio
					.newSound(Gdx.files.internal("typing2.wav"));
			typingSound3 = Gdx.audio
					.newSound(Gdx.files.internal("typing3.wav"));
			typingSound4 = Gdx.audio
					.newSound(Gdx.files.internal("typing4.wav"));
		}

		public void setEntity(Entity entity) {
			this.entity = entity;
			collision = entity.getComponent(CollisionComponent.class);
			score = entity.getComponent(ScoreComponent.class);
			speed = entity.getComponent(SpeedComponent.class);
			fuel = entity.getComponent(FuelComponent.class);
			resultMessage = "";
		}

		float typingTimer = 0;
		int typingTimerSound = 0;
		boolean shouldType;

		void render() {
			font.draw(main.hudBatch,
					"Velocity Y: " + Math.floor(speed.velocityY),
					getPosition(0).x, getPosition(0).y);
			font.draw(main.hudBatch,
					"Velocity X: " + Math.floor(speed.velocityX),
					getPosition(1).x, getPosition(1).y);
			font.draw(main.hudBatch, "Fuel: " + (int) fuel.reservoirLeft + "/"
					+ fuel.reservoirCapacity, getPosition(2).x,
					getPosition(2).y);

			updateResultMessage();
			typingTimer += Gdx.graphics.getDeltaTime()
					* MathUtils.random(6, 20);
			if ((int) typingTimer > typingTimerSound) {
				typingTimerSound++;
				if (MathUtils.randomBoolean())
					typingSound.play();
				else if (MathUtils.randomBoolean())
					typingSound2.play();
				else if (MathUtils.randomBoolean())
					typingSound3.play();
				else if (MathUtils.randomBoolean())
					typingSound4.play();
			}

			if (typingTimer > resultMessage.length())
				typingTimer = resultMessage.length();
			font.draw(main.hudBatch,
					resultMessage.substring(0, (int) Math.floor(typingTimer)),
					4
							/ 2, main.hudViewport.getWorldHeight() / 1f);

		}

		void updateResultMessage() {
			if (fuel.reservoirLeft < 0)
				resultMessage = "No fuel. No survivors.\nMission Failed.";
			if (collision.outCome == CollisionComponent.CRASH)
				resultMessage = 
"Fate has ordained that the men who went to the moon to explore in peace will stay on the moon to rest in peace."
+"\nThese brave men, Neil Armstrong and Edwin Aldrin, know that there is no hope for their recovery. \nBut they also know that there is hope for mankind in their sacrifice."
+"\nThese two men are laying down their lives in mankind's most noble goal: the search for truth and understanding."
+"\nThey will be mourned by their families and friends; they will be mourned by their nation; \nthey will be mourned by the people of the world; \nthey will be mourned by a Mother Earth that dared send two of her sons into the unknown."
+"\nIn their exploration, they stirred the people of the world to feel as one; \nin their sacrifice, they bind more tightly the brotherhood of man."
+"\nIn ancient days, men looked at stars and saw their heroes in the constellations. \nIn modern times, we do much the same, but our heroes are epic men of flesh and blood."
+"\nOthers will follow, and surely find their way home.\nMan's search will not be denied. But these men were the first, and they will remain the foremost in our hearts."
+"\nFor every human being who looks up at the moon in the nights to come will know that \nthere is some corner of another world that is forever mankind.";
			else if (collision.outCome == CollisionComponent.NOT_FLAT)
				resultMessage = "The men who went to the moon in peace will stay on the moon to rest in peace."
						+ "\n\nNixon,\n\n" + Date.valueOf(LocalDate.now());
			else if (collision.outCome == CollisionComponent.PERFECT)
				resultMessage = "Armstrong: Okay. I'm going to step off the LM now.\nArmstrong: That's one small step for man; one giant leap for mankind.\nYour score: "
						+ score.score;
			glyphLayout.setText(font, resultMessage);
		}

		Vector2 getPosition(int index) {
			try {
				return positions.get(index);
			} catch (IndexOutOfBoundsException e) {
				Vector2 v = new Vector2(main.hudViewport.getWorldWidth() - 100,
						lastY - 20);
				positions.add(v);
				lastY -= 20;
				return v;
			}

		}
	}
}
